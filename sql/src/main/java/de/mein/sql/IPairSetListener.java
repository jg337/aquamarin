package de.mein.sql;

/**
 * Created by xor on 29.08.2016.
 */
public interface IPairSetListener<V> {
    V onSetCalled(V value);
}
