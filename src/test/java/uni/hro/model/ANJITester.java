package uni.hro.model;

import java.util.List;

import org.jgap.Chromosome;

import com.anji.integration.Activator;
import com.anji.integration.ActivatorTranscriber;
import com.anji.neat.Evolver;
import com.anji.util.Properties;

public class ANJITester {

	private static ActivatorTranscriber factory;
	
	public static void main(String[] args) throws Throwable {
		//args = new String[1];
		String argsProps = "uni/hro/nn/fish.properties";
		
		// init
		Properties props = new Properties(argsProps);
		factory = (ActivatorTranscriber) props.singletonObjectProperty( ActivatorTranscriber.class );
		
		// evolve
		Evolver ev = Evolver.instance(props);
		for (int i = 0; i < 100; i++) {
			List<Chromosome> chroms = ev.nextGeneration();
			int fit = 0;
			for (Chromosome c : chroms) {
				Activator activator = factory.newActivator( c );
				activator.getInputDimension(); // �berpr�fen der Inputs.				
				activator.getOutputDimension(); // �berpr�fen der Outputs.
				double[] inputs = new double[152]; // 7 Inputs, das ist derzeit in der fish.properties so festgelegt (stimulus.size).
				double[] outputs = activator.next(inputs);
				System.out.println(outputs);
				c.setFitnessValue(fit++);
			}
			ev.evolveGeneration();
			System.out.println(chroms);
		}
		ev.finishSimulation();
		
	}
	
}
