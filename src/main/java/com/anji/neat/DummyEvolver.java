package com.anji.neat;

import com.anji.util.Properties;
import org.jgap.Chromosome;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xor on 6/21/17.
 */
public class DummyEvolver extends Evolver {
    public DummyEvolver() {
        super();
    }

    @Override
    public void init(Properties props) throws Exception {
    }

    @Override
    public Evolver run() throws Exception {
        return this;
    }

    @Override
    public List<Chromosome> nextGeneration() {
        return new ArrayList<>();
    }

    @Override
    public void evolveGeneration() {
    }

    @Override
    public void finishSimulation() {
    }

    @Override
    public Chromosome getChamp() {
        return null;
    }

    @Override
    public double getChampAdjustedFitness() {
        return 0;
    }

    @Override
    public double getTargetFitness() {
        return 0;
    }

    @Override
    public double getThresholdFitness() {
        return 0;
    }
}
