package uni.hro.client;

/**
 * 
 */

import uni.hro.model.EnumFoodSize;
import uni.hro.model.Food;

import java.awt.*;
import java.awt.geom.*;

/**
 * @author Joerg
 *
 *         Diese Klasse bestimmt, wie Futter gezeichnet wird.
 */

public class FoodDrawer {

	/**
	 * Diese Methode bestimmt, wie Futter gezeichnet wird.
	 * 
	 * @param graphics
	 *            Grafik des AquariumJPanel
	 * @param food
	 *            Das zu zeichnenede Futter
	 */
	public static void draw(Graphics g, Food food) {

		Graphics2D g2 = (Graphics2D) g;

		int positionX = (int) food.getX();
		int positionY = (int) food.getY();

		/**
		 * SizefactorX / -Y bestimmen die Zeichengroesse des Futters. (Wenn EnumFoodSize = BIG dann 2x2, sonst 1x1
		 */
		double sizefactorX;
		if(food.getFoodSize() == EnumFoodSize.BIG)
			sizefactorX = 2; 
		else 
			sizefactorX = 1;
		double sizefactorY;
		if(food.getFoodSize() == EnumFoodSize.BIG)
			sizefactorY = 2; 
		else 
			sizefactorY = 1;

		Color color = food.getCurrentColor();

		/**
		 *  Verschiebung des Objekts
		 */
		g2.translate(positionX, positionY);

		/**
		 *  Usprungszustand des Objekts
		 */
		AffineTransform atOriginal = g2.getTransform();

		/**
		 *  Skalierung des Objekts
		 */
		AffineTransform at = new AffineTransform();
		at.setToScale(sizefactorX, sizefactorY);
		if (sizefactorX < 0) {
			g2.translate(-100 * sizefactorX, 0);
		}
		/**
		 *  Transformationen anwenden
		 */
		g2.transform(at);

		/**
		 *  Antialiasing aktiviert
		 */
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g2.setColor(color);
		g2.fillRect(0, 0, 3, 3);
		/**
		 *  Transformation zuruecksetzen
		 */
		g2.setTransform(atOriginal);

		/**
		 *  Reset Verschiebung des Objekts
		 */
		g2.translate(-positionX, -positionY);

	}

}