package uni.hro.client;

import uni.hro.model.World;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

/**
 * @author Johann
 *         <p>
 *         This class is used to create a frame in fullscreen mode on any display.
 *         NEW: When we have a stable client-server connection this should only be client sided.
 */
public class Aquarium extends JFrame {

    private static final long serialVersionUID = 1L;
    //private JLabel yLabel = new JLabel();
    private static GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    private JPanel contentPane;
    private JPanel movementPane;
    private AquariumJPanel aquariumJPanel;
    private JLabel xLabel = new JLabel();
    private boolean isOnPi = false;
    
    private Point initialClick;

	private int generation = 1;
  

	/**
     * Create the frame.
     */
    public Aquarium(World world) {
        //TODO: this three lines may be unimportant
        setTitle("Aquamarin");
        setType(Type.UTILITY);
        
        setResizable(false);
        setUndecorated(true); // deletes the frame decoration (the upper bar of the frame)

        if ("4.4.50-v7+".equals(System.getProperty("os.version"))) {
            gd.setFullScreenWindow(this); // GraphicsEnvironment is essential to create a fullscreen application in unix
            isOnPi = true;
        }
        
        setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        contentPane = new JPanel();
        movementPane = new JPanel();
        movementPane.setBackground(new Color(18, 120, 190));
        movementPane.setLayout(new BorderLayout());
        
        /** move the frame around, except on a Raspberry **/
        if (isOnPi==false) {
        movementPane.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                initialClick = e.getPoint();
                getComponentAt(initialClick);
            }
        });        
        movementPane.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {

                // get location of Window
                int thisX = getLocation().x;
                int thisY = getLocation().y;

                // Determine how much the mouse moved since the initial click
                int xMoved = (thisX + e.getX()) - (thisX + initialClick.x);
                int yMoved = (thisY + e.getY()) - (thisY + initialClick.y);

                // Move window to this position
                int X = thisX + xMoved;
                int Y = thisY + yMoved;
                setLocation(X, Y);
            }
        });
        }
        
        contentPane.setBackground(new Color(18, 120, 190));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout());


        //Joerg: adds AquariumJPanel to Contentpane.
        setSize(world.getWidth(), world.getHeight());
        aquariumJPanel = new AquariumJPanel(world);
        contentPane.add(aquariumJPanel, BorderLayout.CENTER);

        //TODO: delete, only for prototyping
        //	xLabel.setFont(new Font("Tahoma", Font.PLAIN, 40));
        //	xLabel.setHorizontalAlignment(SwingConstants.CENTER);
        //	contentPane.add(xLabel, BorderLayout.WEST);

        //	yLabel.setFont(new Font("Tahoma", Font.PLAIN, 40));
        //	yLabel.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(movementPane, BorderLayout.NORTH);
        movementPane.add(xLabel, BorderLayout.WEST);
        xLabel.setForeground(Color.WHITE);
        
        xLabel.setText("  Generation: " + this.getGeneration());        // String showing the width of the screen on the left side (west)


        /**
         * simple Listener, click- or touch-events kill the application
         */

    }

    public int getDisplayHeight() {
        int height = gd.getDisplayMode().getHeight();
        return height;
    }

    public int getDisplayWidth() {
        int width = gd.getDisplayMode().getWidth();
        return width;
    }

    public GraphicsDevice getGD() {
        return gd;
    }

    public AquariumJPanel getaquariumJPanel() {
        return aquariumJPanel;

    }

    public void repaint() {
        aquariumJPanel.repaint();
    }

    public int getGeneration() {
  		return generation;
  	}

  	public void setGeneration(int generation) {
  		this.generation = generation;
  	}
  	
  	public void refreshLabel(){
  	   xLabel.setText("  Generation: " + this.getGeneration());
  	}
}

