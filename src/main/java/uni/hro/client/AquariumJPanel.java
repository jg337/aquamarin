package uni.hro.client;

import uni.hro.model.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;

/**
 * @author Joerg, Johann
 *         <p>
 *         Das AquariumJPanel dient als Zeichenfl�che f�r das Aquarium und
 *         alle Lebewesen und Objekte, die sich darin befinden.
 */
public class AquariumJPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Aquariumwasserfarbe
	 */

	float timeDifference = 0.03F; // hiermit kann man festlegen, wie schnell der Wechsel von Tag zu Nacht geschehen soll, je gr��er desto schneller
	float factorForGettingDarker = 1 - timeDifference;
	float factorForGettingBrighter = 1 + timeDifference;

	int redBGInt = 18;
	float startredBG = redBGInt / 255F;
	float redBG = startredBG;
	
	int greenBGInt = 120;
	float startgreenBG = greenBGInt / 255F;
	float greenBG = startgreenBG;
	
	int blueBGInt = 190;
	float startblueBG = blueBGInt / 255F;
	float blueBG = startblueBG;
	
	int maxRGB = Math.max(redBGInt, Math.max(greenBGInt, blueBGInt));
	Color backgroundColor = new Color(startredBG, startgreenBG, startblueBG);
	boolean itIsGettingDarker = true;

	/**
	 * Groesse des Fensters
	 */
	int displayWidth;
	int displayHeight;

	/**
	 * Zur Ausgabe der Frames pro Sekunde
	 */
	int frames = 0;
	long frameFor1000ms;
	long frameForBackground;
	long currentFrame;
	int fps;
	/**
	 * Speichern Steine und Pflanzen separat.
	 */
	HashSet<Stone> setOfStones = new HashSet<Stone>();
	HashSet<Plant> setOfPlants = new HashSet<Plant>();
	private int jumpNumberOfGenerations = 0;
	/**
	 * Popupmenu
	 */
	private JPopupMenu pop;
	private World world;
	private boolean fastforward;
	private JPanel panel;
	private boolean popShown = false;

	/**
	 * Konstruktur des AquariumJPanel
	 *
	 * @param world
	 *            Die Welt, in der die Simulation stattfindet.
	 */
	public AquariumJPanel(World world) {
		this.displayWidth = world.getWidth();
		this.displayHeight = world.getHeight();
		this.world = world;
		this.pop = new JPopupMenu();
		menu();

		panel = this;

		prepareDrawing();
		repaint();

		this.addMouseListener(new MouseAdapter() {
			@Override

			public void mouseClicked(MouseEvent e) {
				// open the popup menu, if left click performed
				if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 1 && popShown == false) {
					pop.show(panel, e.getX(), e.getY());
					popShown = true;
				} else {
					// close popup menu, if left click is performed on the the
					// JPanel
					pop.setVisible(false);
					popShown = false;
				}
			}
		});
	}

	public HashSet<Plant> getSetOfPlants() {
		return setOfPlants;
	}

	public void setSetOfPlants(HashSet<Plant> setOfPlants) {
		this.setOfPlants = setOfPlants;
	}

	/**
	 * Alle in der Welt vorkommenden Pflanzen und Steine werden separat
	 * gespeichert.
	 */
	private void prepareDrawing() {

		for (Object s : new HashSet<>(world.getEntities())) {

			if (s instanceof Stone) {

				Stone newStone = (Stone) s;

				setOfStones.add(newStone);
			}

			if (s instanceof Plant) {

				Plant plant = (Plant) s;
				setOfPlants.add(plant);
			}

		}

	}

	/**
	 * Bestimmt den Aufbau des Popup-Menus
	 */
	public void menu() {
		JSeparator sep = new JSeparator();
		JMenuItem informationItem = new JMenuItem("Show Information (TODO)");
		JMenuItem jumpFive = new JMenuItem("Jump 5 Generations");
		JMenuItem jumpOneHundred = new JMenuItem("Jump 25 Generations");
		JMenuItem jumpFiveHundred = new JMenuItem("Jump 100 Generations");
		JMenuItem jumpOneThousand = new JMenuItem("Jump 200 Generations");
		JSeparator sep2 = new JSeparator();
		JMenuItem closeWindowItem = new JMenuItem("End Simulation");

		pop.add(informationItem);
		pop.add(sep);
		pop.add(jumpFive);
		pop.add(jumpOneHundred);
		pop.add(jumpFiveHundred);
		pop.add(jumpOneThousand);
		pop.add(sep2);
		pop.add(closeWindowItem);

		closeWindowItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				System.exit(0);
			}
		});

		jumpFive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {

				fastforward = true;
				setJumpNumberOfGenerations(5);
				pop.setVisible(false);
				popShown = false;
			}
		});

		jumpOneHundred.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				fastforward = true;
				setJumpNumberOfGenerations(25);
				pop.setVisible(false);
				popShown = false;
			}
		});

		jumpFiveHundred.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				fastforward = true;
				setJumpNumberOfGenerations(100);
				pop.setVisible(false);
				popShown = false;
			}
		});

		jumpOneThousand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				fastforward = true;
				setJumpNumberOfGenerations(200);
				pop.setVisible(false);
				popShown = false;
			}
		});

	}

	// @Override

	/**
	 * Gibt an, was auf das AquariumJPanel gezeichnet werden soll.
	 */
	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		setBackground(backgroundColor);

		// nun in paint() / update() bzw. paintComponent() ...
		frames++;
		currentFrame = System.currentTimeMillis();

		// Hintergrund-Farbe anpassen
		calculateNightAndDayBackgroundBehavior();

		// Framerate alle 1000ms schreiben
		if (currentFrame > frameFor1000ms + 1000) {
			frameFor1000ms = currentFrame;
			fps = frames;
			frames = 0;			
		}

		String fpss = "FPS: " + fps;
		g.setColor(Color.WHITE);
		g.drawString(fpss, 0, 10);

		/**
		 * Boden zeichnen
		 */
		GroundDrawer.draw(g, displayWidth, displayHeight, new Color(232, 235, 224), 1, 1);

		/**
		 * Zeichnen von Kreaturen u
		 */
		for (Object s : new HashSet<>(world.getEntities())) {

			if (s instanceof Fish) {
				Fish fish = (Fish) s;
				if (fish.isAlive()) {

					FishDrawer.draw(g, fish);
					HitboxDrawer.draw(g, fish);
				} else {
					DeadFishDrawer.draw(g, fish);
				}

			}

			if (s instanceof Kraken) {
				Kraken kraken = (Kraken) s;
				KrakenDrawer.draw(g, kraken);
				if (kraken.isAlive())
					HitboxDrawer.draw(g, kraken);
				else
					DeadKrakenDrawer.draw(g, kraken);
			}

			if (s instanceof Food) {
				Food food = (Food) s;
				FoodDrawer.draw(g, food);
			}
			/**
			 * if (s instanceof Plant) { Plant plant = (Plant) s;
			 * PlantDrawer.draw(g, plant); }
			 *
			 * if (s instanceof Stone) { Stone stone = (Stone) s;
			 * StoneDrawer.draw(g, stone); }
			 */
		}

		/**
		 * Pflanzen und Steine werden aus separaten Listen abgerufen. Die
		 * Objekte in den Hashsets geraten ducheinander und wuerden anderenfalls
		 * auch immer in unterschiedlicher Reihenfolge gezeichnet werden => Mal
		 * waere ein Stein vorne, dann wieder eine Pflanze... usw.
		 */

		for (Stone stone : setOfStones) {

			StoneDrawer.draw(g, stone);
		}

		for (Plant plant : setOfPlants) {

			PlantDrawer.draw(g, plant);
		}

		if (fastforward == true) {

			FastForwardSymbolDrawer.draw(g, displayWidth, displayHeight);
			fastforward = false;
		}
	}

	private void calculateNightAndDayBackgroundBehavior() {
		// alle 200 ms
		if (currentFrame > frameForBackground + 200) {
			frameForBackground = currentFrame;

			if (itIsGettingDarker) {
				redBG = redBG * factorForGettingDarker;
				greenBG = greenBG * factorForGettingDarker;
				blueBG = blueBG * factorForGettingDarker;
				backgroundColor = new Color(redBG, greenBG, blueBG);

				// wenn ein Viertel erreicht ist, dann sprechen wir von Nacht
				if (startredBG / 4.0F > redBG) {
					world.setIsDay(false);
				}
				
				// wenn ein Wert == 0, dann wieder heller werden
				if (backgroundColor.getRed() == 0 || backgroundColor.getGreen() == 0
						|| backgroundColor.getBlue() == 0) {
					itIsGettingDarker = false;
					//System.out.println("wechsel zu hell");
				}
			} else {
				redBG = redBG * factorForGettingBrighter;
				greenBG = greenBG * factorForGettingBrighter;
				blueBG = blueBG * factorForGettingBrighter;

				// wenn ein Viertel erreicht ist, dann sprechen wir von Tag
				if (startredBG / 4.0F < redBG) {
					world.setIsDay(true);
				}
				
				// wenn ein Wert > Anfangswert, dann wieder dunkler werden
				if (redBG > startredBG || greenBG > startgreenBG || blueBG > startblueBG) {
					itIsGettingDarker = true;
					//System.out.println("wechsel zu dunkel");
					backgroundColor = new Color(startredBG, startgreenBG, startblueBG); // == Anfangsfarbe
					// diese Farbe eine Weile lang behalten
				} else {
					backgroundColor = new Color(redBG, greenBG, blueBG);
				}
			}
		}
	}

	/**
	 * @return
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * @return
	 */
	public int getJumpNumberOfGenerations() {
		return jumpNumberOfGenerations;
	}

	/**
	 * @param jumpNumberOfGenerations
	 */
	public void setJumpNumberOfGenerations(int jumpNumberOfGenerations) {
		this.jumpNumberOfGenerations = jumpNumberOfGenerations;
	}

	public boolean isFastforward() {
		return fastforward;
	}

	public void setFastforward(boolean fastforward) {
		this.fastforward = fastforward;
	}
	
	public void resetPanelBackground() {
		redBG = startredBG;		
		greenBG = startgreenBG;		
		blueBG = startblueBG;
		backgroundColor = new Color(startredBG, startgreenBG, startblueBG);
		itIsGettingDarker = true;
		world.setIsDay(true);
	}

}
