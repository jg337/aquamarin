package uni.hro.model;

import java.awt.Color;
import java.util.Random;


/**
 * Created by Er Bü on 10.05.2017.
 */
public class Food extends Consumable {

	protected World world;
	private double sinkspeed;
	private float fuel = 500;
	private EnumFoodSize foodSize;
	private EnumFoodType foodType;
	protected Color color;

	public Food(World world, float x, float y) {
		super(x, y, 2, 2);
		this.foodSize = EnumFoodSize.values()[(int) generateRandom(0,1)];
		this.foodType = EnumFoodType.values()[(int) generateRandom(0,1)];
		this.world = world;
		this.sinkspeed = ((double)generateRandom(2, 7))/10;
		if(this.getFoodType() == EnumFoodType.NORMAL)
			this.color = Color.lightGray;
		else
			this.color = Color.GREEN;
		//this.sinkspeed=0.3;
	}

	public boolean sink() {
		dy = (float) sinkspeed;
		y = y + dy;
		
		if (this.y > world.getHeight()){
		return false;
		}
		
		return true;		
	}
	
	
	public synchronized float emptyFuel() {
		float result = fuel;
		fuel = 0;
		return result;
	}


	public static int generateRandom(int min, int max) {
		Random rn = new Random();
		int randomvalue = rn.nextInt(max - min + 1) + min;
		return randomvalue;
	}
	
	public EnumFoodSize getFoodSize() {
		return this.foodSize;
	}
	
	public EnumFoodType getFoodType() {
		return this.foodType;
	}
	
	/**
	 * �berschreibt getCurrentColor Methode aus WorldEntity - dort zuf�lliger Wert f�r Color festgelegt
	 */
	@Override
	public Color getCurrentColor() {
        return color;
    }
	
	@Override
    public synchronized float consume() {
        if (!consumed) {
            consumed = true;
            if (this.getFoodSize()==EnumFoodSize.BIG)
                return 40f;
            return 25f;
        }
        return 0f;
    }
}
