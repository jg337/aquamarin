package uni.hro.model.neurons;

import uni.hro.model.OutOfBounds;

/**
 * Created by xor on 6/18/17.
 */
public class OutOfBoundsNeuron extends SecondaryNeuron<OutOfBounds> {
    public OutOfBoundsNeuron(InputNeuron inputNeuron, Class<OutOfBounds> clazz) {
        super(inputNeuron, clazz);
    }
}
