package uni.hro.model;

import uni.hro.model.neurons.InputNeuron;

import java.awt.*;

/**
 * Created by Er Bü on 15.04.2017.
 */
public class Kraken extends Creature {


    public Kraken(World world, float x, float y) {
        super(world, 100f, x, y, 30, 50);
        createNeuron(1, 0);
        createNeuron(1, 1);
        createNeuron(1, -1);
        createNeuron(2, 0);
        createNeuron(2, 1);
        createNeuron(2, 2);
        createNeuron(2, -1);
        createNeuron(2, -2);
        createNeuron(3, 0);
        createNeuron(3, 1);
        createNeuron(3, 2);
        createNeuron(3, 3);
        createNeuron(3, -1);
        createNeuron(3, -2);
        createNeuron(3, -3);
        createNeuron(4,0);
        createNeuron(4,1);
        createNeuron(4,2);
        createNeuron(4,3);
        createNeuron(4,4);
        createNeuron(4,-1);
        createNeuron(4,-2);
        createNeuron(4,-3);
        createNeuron(4,-4);
        createNeuron(5,0);
        createNeuron(5,1);
        createNeuron(5,2);
        createNeuron(5,3);
        createNeuron(5,4);
        createNeuron(5,-1);
        createNeuron(5,-2);
        createNeuron(5,-3);
        createNeuron(5,-4);
        createNeuron(0, 1);
        createNeuron(0, -1);
        createNeuron(-1, 0);
        createNeuron(-1, 1);
        createNeuron(-1, -1);
        createNeuron(-2, 0);
        createNeuron(-2, 1);
        createNeuron(-2, 2);
        createNeuron(-2, -1);
        createNeuron(-2, -2);
        createNeuron(-3, 0);
        createNeuron(-3, 1);
        createNeuron(-3, 2);
        createNeuron(-3, 3);
        createNeuron(-3, -1);
        createNeuron(-3, -2);
        createNeuron(-3, -3);
        createNeuron(-4,0);
        createNeuron(-4,1);
        createNeuron(-4,2);
        createNeuron(-4,3);
        createNeuron(-4,4);
        createNeuron(-4,-1);
        createNeuron(-4,-2);
        createNeuron(-4,-3);
        createNeuron(-4,-4);
        createNeuron(-5,0);
        createNeuron(-5,1);
        createNeuron(-5,2);
        createNeuron(-5,3);
        createNeuron(-5,4);
        createNeuron(-5,-1);
        createNeuron(-5,-2);
        createNeuron(-5,-3);
        createNeuron(-5,-4);
        createSecondaryNeurons();
    }

    @Override
    public void handleCollisions() {
        for (WorldEntity worldEntity : collidedEntities){
            if (worldEntity instanceof Fish){
                Fish fish = (Fish) worldEntity;
                addFuel(fish.consume());
            }
        }
    }


    public static Integer calcInputSize(){
        Kraken kraken = new Kraken(new World(1,1),1,1);
        return Creature.calcInputSize(kraken);
    }
}
