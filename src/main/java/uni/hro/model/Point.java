package uni.hro.model;

import de.mein.core.serialize.SerializableEntity;

public abstract class Point<T> implements SerializableEntity{
    private T x,y;

    public Point(T x, T y) {
        this.x = x;
        this.y = y;
    }

    protected Point() {
    }

    public Point setX(T x) {
        this.x = x;
        return this;
    }

    public Point setY(T y) {
        this.y = y;
        return this;
    }

    public T getX() {
        return x;
    }

    public T getY() {
        return y;
    }
}
